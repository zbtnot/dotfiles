if ! [ -f ~/.antigen.zsh ];
then
        echo "Antigen not found. Downloading.."
        curl -s https://raw.githubusercontent.com/zsh-users/antigen/master/bin/antigen.zsh -o ~/.antigen.zsh
        echo "Antigen installed."
fi
source ~/.antigen.zsh
antigen use oh-my-zsh
antigen bundle git
antigen bundle zsh-users/zsh-syntax-highlighting
antigen theme af-magic
antigen apply
eval `keychain --eval --quiet --agents ssh id_rsa`
export PATH=$PATH:~/.local/bin:~/.composer/vendor/bin
export VAGRANT_DEFAULT_PROVIDER=virtualbox
